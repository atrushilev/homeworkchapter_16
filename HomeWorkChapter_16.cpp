// HomeWorkChapter_16.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.

#include <iostream>
#include <ctime>
using namespace std;
#define n 5


int main()
{
    int array[n][n];
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            array[i][j] = i + j;
            cout << array[i][j] << "  "; 
        }
        cout << endl;
    }
    cout << endl;

    int sum = 0;
    struct tm timeinfo;
    time_t now = time(0);
    localtime_s(&timeinfo, &now);
    int day = timeinfo.tm_mday;

    for (int j = 0; j < n; j++)
    {
        sum += array[day % n][j]; 
    }
    cout << sum << endl;
}



